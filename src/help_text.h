/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const int help_sorok_szama=107;
const char help_text[] = "       Snake v1.02\n\nFeed the snake by\ncontrolling its head to\nthe right direction, to\neat the food, to get it\ngrown. Avoid blocks and\nthe tail of the snake.\nEnjoy this classic com-\nputer (calculator) game!\n\n\n    *** Controls ***\n\nPress [F1]\n   to display the help.\nPress [F2] to\n   launch the setup menu.\nPress [MENU] or [EXIT]\n   to return to the main\n   menu and pause the\n   game.\n\nDefault control with the\narrow keys, but you can\nmodify it under the key-\nboard setup menu.\n\nAfter continuing the game\npress any control key to\nstart the game.\n\n\n      *** Setup ***\n\n   Wait: duration in mil-\nliseconds. For example if\nit's set to 500 ms, the\nsnake moves one block in\nevery half seconds.\n   Keyboard setup: you\ncan change here the con-\ntrols. Press the keys\nthat you want to use in\nthe gameplay. [F1]-[F6],\n[MENU] and [EXIT] keys\nare not enabled.\n\n\n     *** Levels ***\n\nThere are 6 built-in le-\nvels, these are origi-\nnally made by Nokia. Fu-\nture releases will sup-\nport custom levels and\nwill contain a built-in\nlevel editor.\n\n\n   *** High scores ***\n\nEach level has a separate\nhigh score page, you can\nreset them one by one by\npressing [F5].\n\n\n      *** Files ***\n\nThis game saves three\nfiles to the main memory.\nYou can find these in\nMemory Manager/\n   Main Memory/\n      @SNAKE\n\nThese files are:\nGAME: contains your cur-\n   rent gameplay. If you\n   delete it, you won't\n   be able to continue\n   your paused game.\nHIGHSCR: contains your\n   high scores. Delete\n   this if you want to\n   reset the high scores.\nSETTINGS:\n   contains your settings\n   If you delete it, your\n   settings are restored\n   to default.\n\n\n     *** Credits ***\n\nDeveloped by Balping\nbalping.official@gmail.com\n(C) balping 2013-2014\n\nThis package is\ndownloadable at\ncemetech.net\nhttp://goo.gl/oJ3AFx\n\nSupport:\nhttp://goo.gl/83SSCx";
