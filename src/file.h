/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <fxcg/file.h>
#include <stdint.h>
#include "hash.h"
#include "des.hhp"


unsigned char dir[] = "@SNAKE";
unsigned char file_settings[] = "SETTINGS";
unsigned char file_game[] = "GAME";
unsigned char file_rekord[] = "HIGHSCR";

unsigned char main_keys[3][8] = {{201,235,183,43,53,48,148,227}, {123,44,68,86,204,150,118,76}, {155,81,18,150,125,217,47,174}};

static unsigned int lastrandom=0x12345678;
unsigned char code_random(unsigned int seed){
	if (seed) lastrandom=seed;
	lastrandom = ( 0x41C64E6D*lastrandom ) + 0x3039;
	return ( (unsigned char)(lastrandom >> 16) );
}

/*void disp_error(int e){
	if(e==0){
		DisplayMessageBox("OK");
	}else{
		unsigned char message[] = "ERROR: ###";
		itoa(e, message+7);
		DisplayMessageBox(message);
	}
}

const unsigned char key[6] = {75, 147, 192, 127, 215, 94};
const unsigned char order[6] = {5, 0, 2, 4, 3, 1};

void encode(unsigned char * buffer){
	unsigned short i;
	unsigned char nbuf[6];
	for(i=0;i<6;i++){
		nbuf[i]=buffer[i] ^ key[i];
		
	}
	for(i=0;i<6;i++){
		buffer[i] = nbuf[order[i]];
		
	}
}

void decode(unsigned char * buffer){
	unsigned short i;
	unsigned char nbuf[6];
	for(i=0;i<6;i++){
		nbuf[i]=buffer[i] ^ key[i];
		
	}
	for(i=0;i<6;i++){
		buffer[order[i]] = nbuf[i];
		
	}
}*/

void save_settings(){
	int size;
	unsigned short szukseges_size=6;
	unsigned char buffer[szukseges_size];

	buffer[0] = GOMB_FEL;
	buffer[1] = GOMB_LE;
	buffer[2] = GOMB_JOBBRA;
	buffer[3] = GOMB_BALRA;
	memcpy(buffer+4, &KOR_MS, sizeof(short));
	//encode(buffer);

	if(MCSGetDlen2(dir, file_settings, &size) != 0){
		//még nem létezik, létrehozzuk
		MCSPutVar2(dir, file_settings, szukseges_size, (void*)buffer);
	}else{
		if(size<szukseges_size){
			//kisebb, mint a szükséges; törlés, majd újra létrehozás
			MCSDelVar2(dir, file_settings);
			MCSPutVar2(dir, file_settings, szukseges_size, (void*)buffer);
		}else{
			MCSOvwDat2(dir, file_settings,szukseges_size, (void*)buffer, 0); 
		}
	}
}

void load_settings(){
	int size;
	unsigned short szukseges_size=6;
	unsigned char buffer[szukseges_size];
	unsigned short temp_ms=defaults.KOR_MS;
	
	//load defaults
	GOMB_BALRA = defaults.GOMB_BALRA;
	GOMB_JOBBRA = defaults.GOMB_JOBBRA;
	GOMB_FEL = defaults.GOMB_FEL;
	GOMB_LE = defaults.GOMB_LE;
	KOR_MS = defaults.KOR_MS;

	if(MCSGetDlen2(dir, file_settings, &size) == 0){
		if(size>=szukseges_size){
			MCSGetData1(0, szukseges_size, (void*)buffer); 
			//decode(buffer);
			if(buffer[0]<=79 && buffer[1]<=79 && buffer[2]<=79 && buffer[3]<=79){
			if(keyboard_valid[buffer[0]] && keyboard_valid[buffer[1]] && keyboard_valid[buffer[2]] && keyboard_valid[buffer[3]]){
			if(buffer[0]!=buffer[1] && buffer[0]!=buffer[2] && buffer[0]!=buffer[3] && buffer[1]!=buffer[2] && buffer[1]!=buffer[3] && buffer[2]!=buffer[3]){
				GOMB_FEL=buffer[0];
				GOMB_LE=buffer[1];
				GOMB_JOBBRA=buffer[2];
				GOMB_BALRA=buffer[3];
			}
			}
			}
			memcpy(&temp_ms, buffer+4, sizeof(short));
			if(0<=temp_ms && temp_ms<=9999){
				KOR_MS = temp_ms;
			}
		}
	}
}


/*void save_game(snake_t s){
	int size;
	unsigned short szukseges_size=1+sizeof(snake_t);
	unsigned char buffer[szukseges_size];

	buffer[0] = game_mode;
	memcpy(buffer+1, &s, szukseges_size);

	if(MCSGetDlen2(dir, file_game, &size) != 0){
		//még nem létezik, létrehozzuk
		MCSPutVar2(dir, file_game, szukseges_size, (void*)buffer);
	}else{
		if(size<szukseges_size){
			//kisebb, mint a szükséges; törlés, majd újra létrehozás
			MCSDelVar2(dir, file_game);
			MCSPutVar2(dir, file_game, szukseges_size, (void*)buffer);
		}else{
			MCSOvwDat2(dir, file_game,szukseges_size, (void*)buffer, 0); 
		}
	}
	//sys_free(buffer);
}

void load_game(snake_t s){
	int size;
	unsigned short szukseges_size=sizeof(snake_t)+1;
	unsigned char buffer[szukseges_size];
	
	if(MCSGetDlen2(dir, file_game, &size) == 0){
		if(size>=szukseges_size){
			MCSGetData1(0, szukseges_size, (void*)buffer); 
			if(buffer[0] == paused || buffer[0] == jatek){
				game_mode = paused;
				memcpy(&s, buffer+1, sizeof(snake_t));
			}else{
				game_mode = end;
			}
		}
	}
	//sys_free(buffer);
}*/

void encode_game(unsigned char * buffer, unsigned short len){
	unsigned short i;
	unsigned int key2;
	memcpy(&key2, buffer+len-4, 4);
	code_random(key2);
	for(i=0; i<len-4; i++){
		buffer[i] ^= code_random(0);
	}
}

void kodol(unsigned char * be, unsigned char *ki, unsigned short * len, int mode){
	if(*len%8){*len += 8-*len%8;}
	ki = (unsigned char)sys_malloc(*len);

	unsigned short i;
	unsigned char k;
	key_set* key_sets = (key_set*)sys_malloc(17*sizeof(key_set));

	for(k=0; k<3; k++){
		if(mode==ENCRYPTION_MODE){
			generate_sub_keys(main_keys[k], key_sets);
		}else{
			generate_sub_keys(main_keys[2-k], key_sets);
		}
		for(i=0; i<*len; i+=8){
			process_message(be+i, ki+i, key_sets, mode);
		}
		memcpy(be, ki, *len);
	}
}

void save_game(snake_t s){
	int size;
	unsigned short palya_size = sizeof(char)*PALYA_WIDTH*PALYA_HEIGHT*3;
	unsigned short szukseges_size=1+sizeof(koord)*4+sizeof(short)*3+1+1+palya_size+1+sizeof(uint32_t);
	unsigned char buffer[szukseges_size];
	unsigned char *p = buffer;

	*p = game_mode; p++;
	memcpy(p, &(s.fej), 4); p+=4;
	memcpy(p, &(s.farok), 4); p+=4;
	memcpy(p, &(s.hossz), 2); p+=2;
	memcpy(p, &(s.pont), 2); p+=2;
	*p = s.menet; p++;
	*p = s.level; p++;
	memcpy(p, &(s.n_ures), 2); p+=2;
	memcpy(p, s.palya, palya_size); p+=palya_size;
	
	
	*p = version; p++;

	uint32_t hash = SuperFastHash(buffer, (int)szukseges_size-sizeof(uint32_t));
	memcpy(p, &hash, sizeof(uint32_t)); p+=sizeof(uint32_t);
	unsigned char * kodolt;
	//kodol(buffer, kodolt, &szukseges_size, ENCRYPTION_MODE);
	encode_game(buffer, szukseges_size);
	kodolt = buffer;

	if(MCSGetDlen2(dir, file_game, &size) != 0){
		//még nem létezik, létrehozzuk
		MCSPutVar2(dir, file_game, szukseges_size, (void*)kodolt);
	}else{
		if(size<szukseges_size){
			//kisebb, mint a szükséges; törlés, majd újra létrehozás
			MCSDelVar2(dir, file_game);
			MCSPutVar2(dir, file_game, szukseges_size, (void*)kodolt);
		}else{
			MCSOvwDat2(dir, file_game,szukseges_size, (void*)kodolt, 0); 
		}
	}
}

void load_game(snake_t * s){
	int size;
	unsigned short palya_size = sizeof(char)*PALYA_WIDTH*PALYA_HEIGHT*3;
	unsigned short szukseges_size=1+sizeof(koord)*4+sizeof(short)*3+1+1+palya_size+1+sizeof(uint32_t);
	unsigned char buffer[szukseges_size];

	if(MCSGetDlen2(dir, file_game, &size) == 0){
		if(size>=szukseges_size){
			MCSGetData1(0, szukseges_size, (void*)buffer);
			unsigned char * dekodolt;
			//unsigned short szukseges_size2 = szukseges_size;
			//kodol(buffer, dekodolt, &szukseges_size2, DECRYPTION_MODE);
			encode_game(buffer, szukseges_size);
			dekodolt = buffer;
			unsigned char *p = dekodolt;
			if(dekodolt[0] == paused || dekodolt[0] == jatek){
				uint32_t hash = SuperFastHash(dekodolt, (int)szukseges_size-sizeof(uint32_t));
				uint32_t hash2;
				memcpy(&hash2, dekodolt+szukseges_size-sizeof(uint32_t), sizeof(uint32_t));
				if(hash==hash2){
					if(buffer[szukseges_size-5] == 102){//verzió ellenőrzés	
						game_mode = paused; p++;
						memcpy(&(s->fej), p, 4); p+=4;
						memcpy(&(s->farok), p, 4); p+=4;
						memcpy(&(s->hossz), p, 2); p+=2;
						memcpy(&(s->pont), p, 2); p+=2;
						s->menet = *p; p++;
						s->level = *p; p++;
						memcpy(&(s->n_ures), p, 2); p+=2;
						memcpy(s->palya, p, palya_size); p+=palya_size;
					}
				}else{
					game_mode = end;
				}
			}else{
				game_mode = end;
			}
		}
	}
}

void save_rekordok(){
	int size;
	unsigned short szukseges_size=sizeof(rekordok)+1+sizeof(uint32_t);//rekordok + version + hash
	unsigned char buffer[szukseges_size];

	memcpy(buffer, &rekordok, sizeof(rekordok));
	buffer[sizeof(rekordok)] = version;

	uint32_t hash = SuperFastHash(buffer, (int)szukseges_size-sizeof(uint32_t));
	memcpy(buffer+szukseges_size-sizeof(uint32_t), &hash, sizeof(uint32_t));
	unsigned char * kodolt;
	encode_game(buffer, szukseges_size);
	kodolt = buffer;

	if(MCSGetDlen2(dir, file_rekord, &size) != 0){
		//még nem létezik, létrehozzuk
		MCSPutVar2(dir, file_rekord, szukseges_size, (void*)kodolt);
	}else{
		if(size<szukseges_size){
			//kisebb, mint a szükséges; törlés, majd újra létrehozás
			MCSDelVar2(dir, file_rekord);
			MCSPutVar2(dir, file_rekord, szukseges_size, (void*)kodolt);
		}else{
			MCSOvwDat2(dir, file_rekord,szukseges_size, (void*)kodolt, 0); 
		}
	}
}

void load_rekordok(){
	int size;
	unsigned short szukseges_size=sizeof(rekordok)+1+sizeof(uint32_t);//rekordok + version + hash
	unsigned char buffer[szukseges_size];
	

	if(MCSGetDlen2(dir, file_rekord, &size) == 0){
		if(size>=szukseges_size){
			MCSGetData1(0, szukseges_size, (void*)buffer);
			unsigned char * dekodolt;
			encode_game(buffer, szukseges_size);
			dekodolt = buffer;
			
			uint32_t hash = SuperFastHash(dekodolt, (int)szukseges_size-sizeof(uint32_t));
			uint32_t hash2;
			memcpy(&hash2, dekodolt+szukseges_size-sizeof(uint32_t), sizeof(uint32_t));
			if(hash==hash2){
				if(buffer[sizeof(rekordok)] == 102){//verzió ellenőrzés
					memcpy(&rekordok, buffer, sizeof(rekordok));
				}
			}
		}
	}
}
