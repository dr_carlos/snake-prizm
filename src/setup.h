/*
	Snake for CASIO PRIZM calculators
	Copyright (C) 2013-2014  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

unsigned short KOR_MS = 100; //hány milisecundum egy kör
int GOMB_BALRA = KEY_PRGM_LEFT;
int GOMB_JOBBRA = KEY_PRGM_RIGHT;
int GOMB_FEL = KEY_PRGM_UP;
int GOMB_LE = KEY_PRGM_DOWN;

struct {
	unsigned short KOR_MS;
	int GOMB_BALRA;
	int GOMB_JOBBRA;
	int GOMB_FEL;
	int GOMB_LE;
} defaults = {100, KEY_PRGM_LEFT, KEY_PRGM_RIGHT, KEY_PRGM_UP, KEY_PRGM_DOWN};

char keyboard_setup();
void setup();

void setup(){
	MsgBoxPush(6);
	char selected=4;//0: wait, 1:keyboard, 2: default, 3: ok, 4:cancel
	int len=4;
	char buffer[len+1];
	int KOR_MS_EDIT=KOR_MS;
	itoa(KOR_MS_EDIT, buffer);
	int start = 0;
	int cursor = 0;
	
	int x=9;
	int y=4;
	int y_px=(y-1)*24;
	int maxpos = x+len-1;
	
	int key;
	char running=1;
	char *c;
	
	char volte_key=0;
	char volte_kor_ms=0;
	char text_volte[2][2] = {" ","*"};
	
	int GOMB_EDIT[4] = {GOMB_BALRA, GOMB_JOBBRA, GOMB_FEL, GOMB_LE};
	OS_InnerWait_ms(200);
	eleje:
	PrintXY(9, 2, "xxSETUP", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(3, y, "xxWait:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(14, y, "xxms", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);

	
	char megy=1;
	while(megy){
		vissza:
		PrintXY(x, y, buffer-2, selected==0, TEXT_COLOR_BLACK);
		PrintXY(3, 5, "xxKeyboard setup", selected==1, TEXT_COLOR_BLACK);
		PrintXY(3, 7, "xxDefault", selected==2, TEXT_COLOR_BLACK);
		PrintXY(11, 7, "xxOK", selected==3, TEXT_COLOR_BLACK);
		PrintXY(14, 7, "xxCancel", selected==4, TEXT_COLOR_BLACK);
		volte_kor_ms=(KOR_MS_EDIT!=KOR_MS);
		volte_key=(GOMB_EDIT[0]!=GOMB_BALRA || GOMB_EDIT[1]!=GOMB_JOBBRA || GOMB_EDIT[2]!=GOMB_FEL || GOMB_EDIT[3]!=GOMB_LE);
		PrintXY(19, 4, text_volte[volte_kor_ms]-2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		PrintXY(19, 5, text_volte[volte_key]-2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
		GetKey(&key);
		switch(key){
			case KEY_CTRL_RIGHT:
			case KEY_CTRL_DOWN:
				selected=(selected+1)%5;
				break;
			case KEY_CTRL_LEFT:
			case KEY_CTRL_UP:
				selected=(selected+4)%5;
				break;
			case KEY_CTRL_EXIT:
				megy=0;
				break;
			case KEY_CTRL_EXE:
				switch(selected){
					case 1:
						MsgBoxPop();
						keyboard_setup(&GOMB_EDIT);
						MsgBoxPush(6);
						goto eleje;
						break;
					case 2:
						KOR_MS_EDIT=defaults.KOR_MS;
						itoa(KOR_MS_EDIT, buffer);
						GOMB_EDIT[0] = defaults.GOMB_BALRA;
						GOMB_EDIT[1] = defaults.GOMB_JOBBRA;
						GOMB_EDIT[2] = defaults.GOMB_FEL;
						GOMB_EDIT[3] = defaults.GOMB_LE;
						break;
					case 3:
						KOR_MS=KOR_MS_EDIT;
						GOMB_BALRA = GOMB_EDIT[0];
						GOMB_JOBBRA = GOMB_EDIT[1];
						GOMB_FEL = GOMB_EDIT[2];
						GOMB_LE = GOMB_EDIT[3];
						megy=0;
						break;
					case 4:
						megy=0;
						break;
					case 0:
						goto edit_wait;
						break;
				}
				break;
			case KEY_CTRL_DEL:
			case KEY_CTRL_AC:
				buffer[0]=0;
				goto edit_wait;
				break;
			case KEY_CHAR_0:
			case KEY_CHAR_1:
			case KEY_CHAR_2:
			case KEY_CHAR_3:
			case KEY_CHAR_4:
			case KEY_CHAR_5:
			case KEY_CHAR_6:
			case KEY_CHAR_7:
			case KEY_CHAR_8:
			case KEY_CHAR_9:
				buffer[0]='0'+key-KEY_CHAR_0;
				buffer[1]=0;
				goto edit_wait;
				break;
		}
	}
	save_settings();
	MsgBoxPop();
	Bdisp_PutDisp_DD();
	return;
	
	/*-------- EDIT WAIT ------------ */
	edit_wait:
	//itoa(KOR_MS_EDIT, buffer);
	running=1;
	start = 0;
	cursor = strlen(buffer);
	if(cursor==4){key=KEY_CTRL_RIGHT;EditMBStringCtrl2(buffer, len+1, &start, &cursor, &key, x, y_px, 1, maxpos);}else{
	DisplayMBString2(0, buffer, start, cursor, 0, x, y_px, maxpos, 0, 0);}
	while(running){
		GetKey(&key);

		if(key == KEY_CTRL_EXE){
			running=0;
			c=buffer;
			KOR_MS_EDIT=0;
			while(*c){
				KOR_MS_EDIT=KOR_MS_EDIT*10+((*c)-'0');
				c++;
			}
			itoa(KOR_MS_EDIT, buffer);
		}else if(key == KEY_CTRL_EXIT){
			running=0;
			itoa(KOR_MS_EDIT, buffer);
		}

		if(key && key < 30000){
			if(key>=KEY_CHAR_0 && key<=KEY_CHAR_9){
				cursor = EditMBStringChar(buffer, len, cursor, key);
				DisplayMBString2(0, buffer, start, cursor, 0, x, y_px, maxpos+1, 0, key);
			}
		}else{
			if(key!=KEY_CTRL_PASTE){
				//EditMBStringCtrl((unsigned char*)buffer, 4, &start, &cursor, &key, 3, 3);
				EditMBStringCtrl2(buffer, len+1, &start, &cursor, &key, x, y_px, 1, maxpos);
				if(GetSetupSetting(0x14)==2){SetSetupSetting(0x14,0);}//clip elkerülése
			}
		}
	}
	Cursor_SetFlashOff();
	goto vissza;
	/*------ END OF EDIT WAIT ---------- */
}

char keyboard_valid[80] = {0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,0};

char keyboard_setup(int * GOMB_EDIT[4]){
	unsigned short k;
	int key;
	int row, column;
	char buffer[20];
	
	int temp;
	char jo, nincsilyen;
	
	MsgBoxPush(6);
	PrintXY(4, 2, "xxKEYBOARD  SETUP", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(3, 4, "xxLeft:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(12, 4, "xxUp:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(3, 5, "xxRight:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	PrintXY(12, 5, "xxDown:", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	
	Bdisp_PutDisp_DD();
	int i, j;
	for(i=0; i<4; i++){
		itoa(GOMB_EDIT[i], buffer);
		PrintXY(9+(i/2)*9, 4+i%2, buffer-2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
	}
	
	
	char selected=1;//0: change, 1: back
	char valtozas=0;
	char megy=1;
	while(megy){
		PrintXY(3, 7, "xxChange", selected==0, TEXT_COLOR_BLACK);
		PrintXY(16, 7, "xxBack", selected==1, TEXT_COLOR_BLACK);
		GetKey(&key);
		switch(key){
			case KEY_CTRL_RIGHT:
			case KEY_CTRL_DOWN:
			case KEY_CTRL_LEFT:
			case KEY_CTRL_UP:
				selected=(selected+1)%2;
				break;
			case KEY_CTRL_EXIT:
				megy=0;
				break;
			case KEY_CTRL_EXE:
				switch(selected){
					case 1:
						megy=0;
						break;
					case 0:
						PrintXY(3, 7, "xxChange", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
						for(i=0; i<4; i++){
							ujra:
							itoa(GOMB_EDIT[i], buffer);
							PrintXY(9+(i/2)*9, 4+i%2, buffer-2, TEXT_MODE_INVERT, TEXT_COLOR_BLACK);
							Bdisp_PutDisp_DD();
							GetKeyWait_OS(&column, &row, KEYWAIT_HALTON_TIMEROFF , 0, 0, &k);
							temp = column*10+row-1;
							jo=(temp<=79);
							if(jo){jo = keyboard_valid[temp];}
							nincsilyen = 1;
							if(jo){
								for(j=0; j<i; j++){
									nincsilyen = nincsilyen && (temp!=GOMB_EDIT[j]);
								}
							}
							PrintXY(3, 3, "xx                 ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
							if(!jo){
								PrintXY(3, 3, "xxInvalid key!", TEXT_MODE_NORMAL, TEXT_COLOR_RED);
							}
							if(!nincsilyen){
								PrintXY(3, 3, "xxKey already used!", TEXT_MODE_NORMAL, TEXT_COLOR_RED);
							}
							if(!jo || !nincsilyen){
								goto ujra;
							}else{
								if(temp!=GOMB_EDIT[i]){valtozas=1;}
								GOMB_EDIT[i] = temp;
							}
							itoa(GOMB_EDIT[i], buffer);
							PrintXY(9+(i/2)*9, 4+i%2, buffer-2, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
						}
						selected=(selected+1)%2;
						break;
				}
				break;
		}
	}
	MsgBoxPop();
	return valtozas;
}
